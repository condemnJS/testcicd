<?php


namespace App\Helpers;


interface ThemeStatusInterface
{
    /**
     * Ожидаем модерации
     */
    const PENDING_MODERATION = 1;

    /**
     * Прошла модерацию
     */
    const ACCEPT_MODERATION = 2;

    /**
     * Откланено модераией
     */
    const REJECTED_MODERATION = 3;

    /**
     * Список всех доступных статусов
     */
    const LIST_OF_STATUS = [
        self::PENDING_MODERATION,
        self::ACCEPT_MODERATION,
        self::REJECTED_MODERATION,
    ];
}
