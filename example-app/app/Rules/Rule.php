<?php


namespace App\Rules;


class Rule
{
    /**
     * @param string $model
     * @param string $action
     * @return string
     */
    public function findRule(string $model, string $action)
    {
        $model = ucfirst($model);
        $action = ucfirst($action);

        return "App\Rules\\$model\\$action" . 'Rule';
    }

    /**
     * Регистрация правил валидации
     *
     * @param AbstractRule $abstractRule
     */
    public function registerRules(AbstractRule $abstractRule)
    {
        return $abstractRule->rules();
    }
}
