<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = $this->getAllRoles();

        if (!$roles->contains('user')) {
            DB::table('roles')->insert([
                'name' => 'user',
            ]);
        }

        if (!$roles->contains('admin')) {
            DB::table('roles')->insert([
                'name' => 'admin',
            ]);
        }
    }

    public function getAllRoles()
    {
        $query = DB::table('roles')
            ->select(['name'])
            ->get();

        return $query->map(function ($item) {
            return $item->name;
        });
    }
}
