<?php


namespace App\Rules;


abstract class AbstractRule
{
    /**
     * Правила валидации
     *
     * @return array
     */
    abstract public function rules(): array;
}
