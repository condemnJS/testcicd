@extends('base')

@section('title', 'My themes')

@section('section')
    <div class="container mt-3">
        <div>
            <h2>List of your themes</h2>
            @foreach($themes as $theme)
                <div class="mt-3">
                    <div class="card w-100">
                        <div class="card-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div>
                                    <h4 class="card-title">{{ $theme->name }}</h4>
                                    <p class="card-text">{{ $theme->text }}</p>
                                </div>
                                <div>
                                    @if($theme->status == \App\Models\Theme::PENDING_MODERATION)
                                        <small class="text-primary h3">Pending</small>
                                    @elseif($theme->status == \App\Models\Theme::ACCEPT_MODERATION)
                                        <small class="text-success h3">Accepted</small>
                                    @elseif($theme->status == \App\Models\Theme::REJECTED_MODERATION)
                                        <small class="text-danger h3">Rejected</small>
                                    @endif
                                </div>
                            </div>
                            <div class="d-flex justify-content-between align-items-center mt-3">
                                <a href="{{ url('theme/view/' . $theme->id) }}" class="btn btn-primary">look...</a>
                                <p class="text-secondary">{{ $theme->created_at }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
