<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand brand" href="/">Mini Forum</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav w-100 d-flex justify-content-between">
                <div class="d-flex">
                    @if (auth()->check())
                        <a href="/theme/create" class="nav-link" style="color: #fff !important;">Create theme</a>
                        <a href="/theme/list" class="nav-link" style="color: #fff !important;">My themes</a>
                    @endif
                </div>
                <div class="d-flex">
                    @if (!auth()->check())
                        <a class="nav-link" aria-current="page" href="/user/register">Registration</a>
                        <a class="nav-link" href="/user/login">Login</a>
                    @else
                        <form action="{{ url('/user/logout') }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="nav-link button_nav">Logout</button>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</nav>
