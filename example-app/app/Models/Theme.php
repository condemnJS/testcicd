<?php

namespace App\Models;

use App\Helpers\ThemeStatusInterface;
use App\Helpers\UserHelper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Theme extends Model implements ThemeStatusInterface
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'text',
        'user_id',
        'status',
    ];

    /**
     * Создание темы
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function create(Request $request)
    {
        if ($request->method() === $request::METHOD_POST) {

            if (UserHelper::userIsBaned()) {
                return redirect()->back()->with('error', 'You are banned');
            }

            $theme = new self();

            $theme->name = $request->name;
            $theme->text = $request->text;
            $theme->user_id = Auth::id();
            $theme->status = self::PENDING_MODERATION;

            $theme->save();

            return redirect('/')->with('message', 'Successful create theme');
        }

        return view('theme/create');
    }

    /**
     * Список тем пользователя
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function list(Request $request)
    {
        $themes = collect(self::where('user_id', Auth::id())->get());

        $themes->sort(function ($a, $b) {
            if ($a->status == $b->status) {
                return 0;
            }

            return ($a->status < $b->status) ? 1 : -1;
        });

        return view('theme/list', ['themes' => $themes]);
    }

    /**
     * Список всех тем для админа
     *
     * @param Request $request
     */
    public function listForAdmin(Request $request)
    {
        $themes = Theme::all();

        $themes->sort(function ($a, $b) {
            if ($a->status == $b->status) {
                return 0;
            }

            return ($a->status < $b->status) ? 1 : -1;
        });

        return view('theme/listForAdmin', ['themes' => $themes]);
    }

    /**
     * Обновление статуса
     *
     * @param Request $request
     * @param string $params
     */
    public function updateStatus(Request $request, string $params)
    {
        $params = json_decode($params);
        $theme = Theme::find($params->themeId);

        if ($theme->status == $params->status) {
            return redirect()->back()->with('message', 'The status is already in use');
        }

        $theme->status = $params->status;
        $theme->save();

        return redirect()->back()->with('message', 'Change statuses');
    }

    /**
     * Посмотреть конкретную тему
     *
     * @param Request $request
     * @param string $themeId
     */
    public function view(Request $request, string $themeId)
    {
        $theme = Theme::find($themeId);

        return view('theme/view', ['theme' => $theme]);
    }
}
