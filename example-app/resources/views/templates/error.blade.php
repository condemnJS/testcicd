@if (session()->has('error'))
    <div class="container">
        <div class="alert alert-danger mt-3" role="alert">
            {{ session()->get('error') }}
        </div>
    </div>
@endif
