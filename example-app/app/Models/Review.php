<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Review extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'theme_id',
        'user_id',
    ];

    /**
     * Отправить комментарий
     *
     * @param Request $request
     * @param string $themeId
     */
    public function send(Request $request, string $themeId)
    {
        dd('fired');
        Review::create([
            'text' => $request->text,
            'user_id' => Auth::id(),
            'theme_id' => $themeId,
        ]);
    }
}
