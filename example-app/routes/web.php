<?php

use App\Http\Controllers\DefaultController;
use App\Models\Theme;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $themes = Theme::all();

    return view('welcome', ['themes' => $themes]);
});

// Ограничиваем количество запросов с одного IP адреса
Route::middleware(['throttle:3,1'])->group(function () {
    Route::get('/', function () {
       return 'Hello World!';
    });

//    Route::any('/{model}/{action}/{params?}', [DefaultController::class, 'init']);
});


