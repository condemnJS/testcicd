@if (session()->has('message'))
    <div class="container">
        <div class="alert alert-success mt-3" role="alert">
            {{ session()->get('message') }}
        </div>
    </div>
@endif
