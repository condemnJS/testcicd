<?php


namespace App\Helpers;


use Illuminate\Support\Facades\Auth;

class UserHelper
{
    /**
     * Проверка, что пользователь забанен
     */
    public static function userIsBaned()
    {
        return Auth::user()->is_baned;
    }
}
