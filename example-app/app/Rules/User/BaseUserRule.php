<?php


namespace App\Rules\User;


use App\Rules\AbstractRule;

class BaseUserRule extends AbstractRule
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:6|max:64'
        ];
    }
}
