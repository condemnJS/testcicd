<?php

namespace App\Http\Controllers;

use App\Rules\AbstractRule;
use App\Rules\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DefaultController extends Controller
{
    /**
     * Methods (GET, POST, DELETE, UPDATE)
     *
     * @var string
     */
    private $method;

    /**
     * First param (title file)
     *
     * @var string
     */
    private $model;

    /**
     * Second param (title method)
     *
     * @var string
     */
    private $action;


    /**
     * Init path to model and run her method
     *
     * @param Request $request
     * @param string $model name model
     * @param string $action name method
     * @param mixed $params params
     */
    public function init(Request $request, string $model, string $action, ...$params)
    {
        try {
            $this->model = $this->findModel($model);
            $this->action = $action;
            $this->method = $request->getMethod();

            switch ($this->method) {
                case $request::METHOD_POST:
                    $validator = Validator::make($request->all(), $this->getRules($model));

                    if ($validator->fails()) {
                        return redirect()
                            ->back()
                            ->withErrors($validator);
                    }
                    break;
            }

            $object = new $this->model();
            return call_user_func_array([$object, $action], [$request, ...$params]);

        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }

    /**
     * @param string $model
     * @return mixed
     */
    private function findModel(string $model)
    {
        $model = ucfirst($model);
        return "App\Models\\$model";
    }

    /**
     * Получить правила валидации в зависимости от роута
     *
     * @param string $model
     * @return array
     */
    protected function getRules(string $model): array
    {
        $instance = new Rule();
        $findRule = $instance->findRule($model, $this->action);

        if (class_exists($findRule)) {
            $rule = new $findRule();
            return $instance->registerRules($rule);
        }

        return [];
    }
}
