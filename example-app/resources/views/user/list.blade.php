@extends('base')

@section('title', 'Пользователи')

@section('section')
    <div class="container">
        <div class="mt-3">
            <h2>Пользователи</h2>
            <table class="table table-striped table-dark mt-3 w-100">
                <thead style="background: #212529;">
                    <tr class="w-100 d-flex align-items-center justify-content-between">
                        <th scope="col">#</th>
                        <th scope="col">Email</th>
                        <th scope="col">First name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">isBanned</th>
                    </tr>
                </thead>
                <tbody class="w-100">
                @foreach($users as $key => $val)
                    <tr class="d-flex w-100 align-items-center justify-content-between" style="background: #2c3034;">
                        <th scope="row">{{ $val->id }}</th>
                        <td>{{ $val->email }}</td>
                        <td>{{ $val->username }}</td>
                        <td>{{ $val->surname }}</td>
                        @if ($val->is_baned)
                            <td>
                                <form action="{{ url('user/unban', ['userId' => $val->id]) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-success">Разбанить</button>
                                </form>
                            </td>
                        @else
                            <td>
                                <form action="{{ url('user/ban', ['userId' => $val->id]) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Забанить</button>
                                </form>
                            </td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
