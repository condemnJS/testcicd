<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use HasApiTokens, HasFactory, Notifiable;
    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'username',
        'surname',
        'email',
        'password',
        'role_id',
        'is_baned',
    ];

    /**
     * Мутатор пароля
     */
    public function setPasswordAttribute(string $password)
    {
        return $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Регистрация
     *
     * @param Request $request
     */
    public function register(Request $request)
    {
        if ($request->method() === $request::METHOD_POST) {
            self::create([
                'username' => $request->username,
                'surname' => $request->surname,
                'email' => $request->email,
                'password' => $request->password,
                'role_id' => Role::getUserRoleId(),
                'is_baned' => false,
            ]);

            return redirect('/user/login')->with('message', 'Successful registration!');
        }

        return view('user/register');
    }

    /**
     * Авторизация
     *
     * @param Request $request
     */
    public function login(Request $request)
    {
        if ($request->method() === $request::METHOD_POST) {
            if (!Auth::attempt($request->only(['email', 'password']))) {
                return redirect()->back()->with('error', 'Incorrect email or password');
            }

            return redirect('/')->with('message', 'Successful auth');
        }

        return view('user/login');
    }

    /**
     * Выход из системы
     */
    public function logout()
    {
        Auth::logout();

        return redirect('/')->with('message', 'Successful logout');
    }

    /**
     * Получить список пользователей
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $users = User::all();

        return view('user/list', ['users' => $users]);
    }

    /**
     * Забанить пользователя
     *
     * @param Request $request
     * @param string $userId
     */
    public function ban(Request $request, string $userId)
    {
        $user = self::find($userId);

        $user->is_baned = true;
        $user->save();

        return redirect('/user/list');
    }

    /**
     * Разбанить пользователя
     *
     * @param Request $request
     * @param string $userId
     */
    public function unban(Request $request, string $userId)
    {
        $user = self::find($userId);

        $user->is_baned = false;
        $user->save();

        return redirect('/user/list');
    }
}
