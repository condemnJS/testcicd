<?php


namespace App\Rules\Review;


use App\Rules\AbstractRule;

class SendRule extends AbstractRule
{
    public function rules(): array
    {
        return [
            'text' => 'required',
        ];
    }
}
