<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public static function getUserRoleId()
    {
        return self::select('id')
            ->where('name', 'user')
            ->first()['id'];
    }
}
