<?php


namespace App\Rules\User;


class RegisterRule extends BaseUserRule
{
    public function rules(): array
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'username' => 'required',
            'surname' => 'required',
            'password' => 'required|min:6|max:64|confirmed',
        ];

        return array_merge(parent::rules(), $rules);
    }
}
