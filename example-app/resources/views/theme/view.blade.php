@extends('base')

@section('title', $theme->name)

@section('section')
    <div class="container mt-3">
        <div class="card">
            <div class="card-title mt-3" style="margin-left: 20px">
                <h2>{{ $theme->name }}</h2>
            </div>
            <div class="card-body">
                {{ $theme->text }}
                <p class="text-muted mt-3">{{ $theme->created_at }}</p>
            </div>
        </div>

        <div class="mt-3">
            <form action="{{ url('review/send', $theme->id) }}" method="POST">
                @csrf
                <h4>Написать комментарий</h4>
                <textarea name="text" id="" cols="10" rows="10" class="form-control @error('text') is-invalid @enderror"></textarea>
                @error('text')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                <button type="submit" class="btn btn-primary mt-3">Send</button>
            </form>
        </div>
        <div>
            <h3 class="mt-3">Комментарии:</h3>
        </div>
    </div>
@endsection
