@extends('base')

@section('title', 'Create theme')

@section('section')
    <div class="container mt-3">
        <h2>Create Theme</h2>
        <form method="POST" action="{{ url('/theme/create') }}">
            @csrf
            <div class="mb-3">
                <label class="form-label">Name theme</label>
                <input type="text" name="name" class="form-control @error('name') is-invalid @enderror">
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label class="form-label">Text</label>
                <textarea name="text" cols="30" rows="10" class="form-control @error('text') is-invalid @enderror"></textarea>
                @error('text')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-success">Create</button>
        </form>
    </div>
@endsection
