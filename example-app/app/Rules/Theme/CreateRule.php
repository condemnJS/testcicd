<?php


namespace App\Rules\Theme;


use App\Rules\AbstractRule;

class CreateRule extends AbstractRule
{

    public function rules(): array
    {
        return [
            'name' => 'required',
            'text' => 'required',
        ];
    }
}
